/*
 *Fecha: 19-08-2020
 *Autor: Fatia Azucena MC
 *fatimaazucenamartinez274@gmail.com*/

/*Un vendedor ha realizado N ventas y desea saber cuántas fueron por 10000 o menos, cuántas fueron por más de 10000 pero por menos de 20000, y cuánto fue el monto de las ventas de cada una y el monto global. Realice un algoritmo para determinar los totales.*/

/*Libreria principal*/
#include <iostream>
using namespace std;
    

int main(){
    /*Definicion de variables*/
    int numVenta;
    float montoGlobal=0;
    float montoMADE=0;
    float montoMEDE=0;
    int p;
    int v;
    /*Ingresa datos por teclado*/
    cout<<"Ingrese numero de ventas:"<<'\n';
    cin>>numVenta;
    int g[numVenta];
    /*Inicio for*/
    for(p=1;p<=numVenta;p++){
        cout<<"Ingrese el precio de cada venta:"<<'\n';
        cin>> g[p];
    }/*FIn for*/
    /*Inicio segundo for*/
    for(v=1;v<=numVenta;v++){
        if (g[v] <=10000){
            montoMEDE=montoMEDE + g[v];
            montoGlobal=montoGlobal + g[v];
        }/*Fin segundo for*/
        else if (g[v] >=10000 && g[v]< 20000){
            montoMEDE=montoMEDE + g[v];
            montoGlobal=montoGlobal + g[v];
        }
    }
    cout<<"El monto global es: "<<montoMADE<<" + "<<montoMEDE<<" = "<<montoGlobal;
}/*Fin main*/
