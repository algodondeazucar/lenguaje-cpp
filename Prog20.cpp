/*Fecha: 14-08-2020
 * Autor:Fatima Azucena MC
 * fatimaazucenamartinez274@gmail.com */

/*Se requiere determinar el tiempo que tarda una persona en llegar de una ciudad a otra en bicicleta, considerando que lleva una velocidad constante. Realice un diagrama de flujo y pseudocódigo que repre­senten el algoritmo para tal fin.*/

/*Libreria principal*/
#include <iostream>
using namespace std;

int main(){/*Inicio metodo principal*/
	/*Declaracion de variables*/
	float tiempoLlegada;
	float kRecorridos;
	float kHora;

	/*Entrada de datos por usuario*/
	cout<<"Ingrese los kilometro recorridos: ";
	cin>>kRecorridos;
	cout<<"Ingrese los K/H: ";
	cin>>kHora;
	/*Operacion para obtener el tiempo que tardara*/
	tiempoLlegada=(kRecorridos/kHora);
        /*Se despliega el resultado*/
	cout<<"El tiempo que tarda en llegar la persona es de: "<<tiempoLlegada<<" horas";
}/*Fin de metodo principal*/
