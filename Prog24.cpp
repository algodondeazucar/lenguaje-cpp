/*
 *Fecha: 25-08-2020
 *Autor: Fatima Azucena MC
 *fatimaazucenamartinez274@gmail.com*/

/*La CONAGUA requiere determinar el pago que debe realizar una persona por el total de metros cúbicos que consume de agua.*/

/*Libreria prncipal*/
#include <iostream>
using namespace std;
int main(){/*Inicio metodo principal*/
	/*Declaracion de variables*/
	double precioMetro;
	double resultado;
	double metros;

	cout<<"Ingrese el precio por el metro cubico: ";
	cin>>precioMetro;
	cout<<"Ingrese los litros cubicos de agua consumidos: ";
	cin>>metros;

	resultado=(precioMetro*metros);

	cout<<"El total a pagar es de: $"<<resultado<<" pesos";
}/*Fin metodo principal*/
