/*
 *Fecha: 29_09_20
 *Autor: Fátima Azucena MC
 <fatimaazucenamartinez274@gmail.com>*/

/*El banco “Bandido de peluche” desea calcular para cada 
uno de sus N clientes su saldo actual, su pago mínimo y su 
pago para no generar intereses. Además, quiere calcular el
monto de lo que ganó por con­cepto interés con los clientes 
morosos. Los datos que se conocen de cada cliente son: saldo 
anterior, monto de las compras que realizó y pago que depositó 
en el corte anterior. Para calcular el pago mínimo se considera 
15% del saldo actual, y el pago para no generar intereses corresponde 
a 85% del saldo actual, considerando que el saldo actual debe incluir 
12% de los intereses causados por no realizar el pago mínimo y $200
de multa por el mismo motivo. Realice el algo­ritmo correspondiente 
y represéntelo mediante diagrama de flujo y pseudocódigo.*/
#include <iostream>
using namespace std;

int main(){/*Inicio metodo principal*/
	/*Declaración de variables*/
	float saldoAnterior = 0;
	float pagoActual = 0;
	float saldoActual = 0;
	float pagoMinimo = 0;
	float pagoInteres = 0;
	float montoCompras = 0;
	float depositoAnterior = 0;
	int numClientes;
	string nombre;

	cout<<"Ingrese la cantidad de clientes: ";
	cin>>numClientes;

	for(int i=0;i<=numClientes;i++){/*Inicio for_1*/
	    cout<<"Ingrese el nombre del cliente "<<i<<": ";
	    cin>>nombre;
	    cout<<"Ingrese el saldo anterior: ";
	    cin>>saldoAnterior;
	    cout<<"Ingrese su ultimo deposito: ";
	    cin>>depositoAnterior;
	    cout<<"Ingrese el monto por sus compras realizadas: ";
	    cin>>montoCompras;
	    cout<<"Ingrese el saldo actual: ";
	    cin>>saldoActual;

	    pagoActual = (saldoActual*0.12)+200;
	    pagoMinimo = (saldoActual*0.15);
	    pagoInteres = (saldoActual*0.85);

	    cout<<"El saldo actual de "<<nombre<<" es de: $"<<pagoActual<<" pesos"<<"\n";
	    cout<<"El saldo minimo de "<<nombre<<" es de: $"<<pagoMinimo<<" pesos"<<"\n";
	    cout<<"El pago para no generar intereses de  "<<nombre<<" es de: $"<<pagoInteres<<" pesos"<<"\n";
       }/*Fin for_1*/

}/*Fin método principal*/
