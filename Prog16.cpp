/*
 * Fecha:12-08-2020
 * Autor:Fatima Azucena MC
 * fatimaazucenamartinez274@gmail.com */

/*Realice un algoritmo que, con base en una calificación proporciona­da (0-10), indique con letra la calificación que le corresponde: 10 es “A”, 9 es “B”, 8 es “C”, 7 y 6 son “D”, y de 5 a 0 son “F”. Represente el diagrama de flujo, el pseudocódigo correspon­diente.
*/

/*Lireria principal*/
#include <iostream>
using namespace std;

int main(){/*Inicio de metodo principal*/

	/*Declaracion de variables*/
        int opcion;
	cout<<"Ingrese su calificacion: ";
	cin>>opcion;

	switch (opcion){/*Inicio switch-case*/
		case 10:
			cout<<"Sacaste A :D";
		break;
		case 9:
			cout<<"Sacast B :D";
		break;
		case 8:
			cout<<"Sacaste C :D";
		break;
		case 7:
			cout<<"Sacaste D :)";
		break;
		case 6:
			cout<<"Sacaste D :|";
		break;
		case 5:
			cout<<"Sacaste F :(";
		break;
		case 4:
			cout<<"Sacaste F :(";
		break;
		case 3:
			cout<<"Sacate F :(";
		break;
		case 2:
			cout<<"Sacaste F :(";
		break;
		case 1:
			cout<<"Sacaste F :(";
		break;
		case 0:
			cout<<"Sacaste F :(";
		break;
		default:
			cout<<"Opcion no valida";
		break;

	}/*Fin switch-case*/
}/*Fin de meeeeetodo principal*/
