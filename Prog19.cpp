/*Fecha: 13-08-2020
 *Autor: Fatima Azucena MC
 *fatimaazucenamartinez274@gmail.com */

/*Realice un algoritmo para determinar qué cantidad de dinero hay en un monedero, considerando que se tienen monedas de diez, cin­co y un peso, y billetes de diez, veinte y cincuenta pesos. Represén­telo mediante diagrama de flujo, pseudocódigo.*/

/*Libreria principal*/
#include <iostream>
using namespace std;

int main(){/*Inicio metodo principal*/
        /*Declaracion de variables*/
       	int convertidor=0;
        float resultado=0;
	float monedero[ ] = { 1000, 500, 200, 100, 50, 20, 10, 5, 2, 1, .50, .20, .10 };

        for(int x=0;x<=12;x++){/*Inicio for 1*/
                cout<<"Ingrese la cantidad de  $"<<monedero[x]<<" pesos que tiene : "<<"\n";
		cin>>convertidor;
                resultado=resultado+(convertidor*monedero[x]);
        }/*Fin for 1*/
        cout<<"El total almacenade es de: $"<<resultado<<" pesos";
}/*Fin de metodo principal*/
