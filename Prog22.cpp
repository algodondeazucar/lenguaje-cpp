/*
 *Fecha: 20-08-2020
 *Autor: Fatima Azucena MC 
 *fatimaazucenamartinez274@gmail.com*/

/*Se requiere determinar el costo que tendrá realizar una llamada te­lefónica con base en el tiempo que dura la llamada y en el costo por minuto.*/

/*Libreria principal*/
#include <iostream>
using namespace std;

int main(){/*Inicio metodo principal*/
	/*Declaracion de variables*/
	double precioMinuto;
	int minuto;
	double resultado;

	cout<<"Ingrese el precio por minuto: ";
	cin>>precioMinuto;
	cout<<"Ingrese los minutos que tomo su llamada: ";
	cin>>minuto;

	resultado=(precioMinuto*minuto);

	cout<<"El coso de su llamada es de: $"<<resultado<<" pesos"<<"\n";
}/*Fin metodo principal*/
