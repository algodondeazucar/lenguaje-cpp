/*
 *Fecha: 11_09_2020
 *Autor: Fatima Azucena MC
 *fatimaazucenamartinez274@gmail.com */

/*25-Realice un algoritmo que determine el sueldo semanal de N trabajadores 
considerando que se les descuenta 5% de su sueldo si ganan entre 0 y 150 pesos.
Se les descuenta 7% si ganan más de 150 pero menos de 300, y 9% si ganan más de 
300 pero menos de 450. Los datos son horas trabajadas, sueldo por hora y nombre 
de cada trabajador. Represéntelo mediante diagrama de flujo, pseudocódigo.*/

/*Libreria principal*/
#include <iostream>
using namespace std;

int main(){/*Metodo principal*/
	/*Declaracion de variables*/
        float sueldoHoras;
	int canTrabajadores;
	string nombre;
	int hTrabajadas;
	float dineroGanado;
	float sueldoBruto;
	float sueldoNeto;
	

	/*Entrada de datos por teclado*/
	cout<<"Inserte el precio por hora: ";
	cin>>sueldoHoras;
	cout<<"Inserte el numero de trabajadores: ";
	cin>>canTrabajadores;

	for(int i=1;i<=canTrabajadores;i++){
	    cout<<"Inserte el nombre del trabajador "<<i<<": ";
	    cin>>nombre;
	    cout<<"Inserte las horas trabajadas: ";
	    cin>>hTrabajadas;
	    cout<<"Inserte el dinero ganado: ";
	    cin>>dineroGanado;

	    /*Inicio condicional if-else-if*/
	    if((dineroGanado>)&&(dineroGanado<=150)){
	    	sueldoBruto=40*(dineroGanado/hTrabajadas);
		sueldoNeto=sueldoNeto-(sueldoNeto*0.05);
	
	    }
	     else if ((dineroGanado>150)&&(dineroGanado<=350)){
	     	sueldoBruto=40*(dineroGanado/hTrabajadas);
		sueldoNeto=sueldoNeto-(sueldoNeto*0.07);
			
	     }
	      else if ((dineroGanado>=350)&&(dineroGanado<=450)){
	      	sueldoBruto=40*(dineroGanado/hTrabajadas);
		sueldoNeto=sueldoNeto-(sueldoNeto*0.09);
	       }

	      cout<<"El sueldo bruto de "<<nombre<<" es de: $"<<sueldoBruto<<" pesos"<<"\n";
	      cout<<"El sueldo neto de "<<nombre<<" es de: $"<<sueldoNeto<<" pesos"<<"\n";
	}
}/*Fin metodo principal*/


